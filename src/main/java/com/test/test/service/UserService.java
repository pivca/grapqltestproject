package com.test.test.service;

import com.test.test.dto.UserInputDTO;
import com.test.test.model.User;

import java.util.List;

public interface UserService {
     User findById(int id);
     List<User> findAll();
     User createUser(UserInputDTO userInputDTO);
     User updateUser(Integer id , UserInputDTO userInputDTO);
     User deleteUser(Integer id);

}
