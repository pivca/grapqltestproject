package com.test.test.datafetcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsQuery;
import com.test.test.dto.UserInputDTO;
import com.test.test.model.User;
import com.test.test.service.UserServiceImpl;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@DgsComponent
public class UserDatafetcher {

    @Autowired
    UserServiceImpl userServiceImpl;
    @DgsQuery
    public User user(int id){
        return this.userServiceImpl.findById(id);
    }
    @DgsQuery
    public List<User> users(){
        return this.userServiceImpl.findAll();
    }
    @DgsData(parentType = "Mutation", field = "createUser")
    public User createUser(DataFetchingEnvironment dataFetchingEnvironment){

        Map<String,Object> input = dataFetchingEnvironment.getArgument("input");
        UserInputDTO inputDTO = new ObjectMapper().convertValue(input, UserInputDTO.class);
        return this.userServiceImpl.createUser(inputDTO);
    }
    @DgsData(parentType = "Mutation", field = "updateUser")
    public User updateUser(DataFetchingEnvironment dataFetchingEnvironment){

        Map<String,Object> input = dataFetchingEnvironment.getArgument("input");
        UserInputDTO inputDTO = new ObjectMapper().convertValue(input, UserInputDTO.class);
        Integer id = Integer.parseInt(dataFetchingEnvironment.getArgument("id"));
        return this.userServiceImpl.updateUser(id,inputDTO);
    }
    @DgsData(parentType = "Mutation", field = "deleteUser")
    public User deleteUser(DataFetchingEnvironment dataFetchingEnvironment){


        Integer id = Integer.parseInt(dataFetchingEnvironment.getArgument("id"));
        return this.userServiceImpl.deleteUser(id);
    }
}
