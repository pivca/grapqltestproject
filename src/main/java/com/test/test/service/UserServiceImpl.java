package com.test.test.service;

import com.test.test.dto.UserInputDTO;
import com.test.test.exception.EntityNotFoundException;
import com.test.test.exception.UserAlreadyExistsException;
import com.test.test.model.User;
import com.test.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;



    @Override
    public User findById(int id){
        return this.userRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(String.format("User with the given id: %s not found", id), HttpStatus.NOT_FOUND.toString()));
    }
    @Override
    public List<User> findAll(){
        return this.userRepository.findAll();
    }
    @Override
    public User createUser(UserInputDTO userInputDTO){
        User user= new User();
        if (userRepository.findByEmail(userInputDTO.getEmail()).isPresent())
            throw new UserAlreadyExistsException(String.format("email: %s is taken", userInputDTO.getEmail()), String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        user.setEmail(userInputDTO.getEmail());
        user.setName(userInputDTO.getName());

        user= this.userRepository.save(user);
        return user;
    }
    @Override
    public User updateUser(Integer id , UserInputDTO userInputDTO){
        User user = this.userRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(String.format("User with the given id: %s not found", id), HttpStatus.NOT_FOUND.toString()));
        user.setEmail(userInputDTO.getEmail());
        user.setName(userInputDTO.getName());

        user= this.userRepository.save(user);
        return user;

    }
    @Override
    public User deleteUser(Integer id){
        User user = this.userRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(String.format("User with the given id: %s not found", id), HttpStatus.NOT_FOUND.toString()));
        this.userRepository.delete(user);
        return user;
    }

}
