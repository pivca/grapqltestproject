package com.test.test.exception;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EntityNotFoundException extends RuntimeException {
    private ValidationError validationError;

    public EntityNotFoundException(String message, String code) {
        super(message);
        validationError = new ValidationError(message, code);
    }
}